﻿/*
    Copyright 2013 abhijeet bhagat
 
    TinyASM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TinyASM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TinyASM.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace TinyASM
{
    interface IInstruction
    {
        string Decode();
    }

    #region Regexp classes

    interface IRegexp
    {
        Tuple<byte, Match> Match(string instructionArgs);
    }

    abstract class Regexp : IRegexp
    {
        Regex regex;
        IRegexp dec;
        byte offset;

        protected Regexp(string _pattern, IRegexp _dec, byte _offset)
        {
            regex = new Regex(_pattern);
            dec = _dec;
            offset = _offset;
        }

        Tuple<byte, Match> IRegexp.Match(string instructionArgs)
        {
            Match m = regex.Match(instructionArgs);
            if (m.Success)
            {
                return new Tuple<byte, Match>(offset, m);
            }
            else
            {
                if (dec != null)
                {
                    return dec.Match(instructionArgs);
                }
                else
                {
                    throw new ArgumentException(String.Format("Invalid args - {0}", instructionArgs));
                }
            }
        }
    }

    class AddressAddress : Regexp
    {
        public AddressAddress(IRegexp _dec) : base("\\[(?<op1>\\d+)\\]\\s+\\[(?<op2>\\d+)\\]", _dec, 0)
        { }
    }

    class AddressLiteral : Regexp
    {
        public AddressLiteral(IRegexp _dec) : base("\\[(?<op1>\\d+)\\]\\s+(?<op2>\\d+)", _dec, 1)
        { }
    }

    class LiteralAddress : Regexp
    {
        public LiteralAddress(IRegexp _dec) : base("(?<op1>\\d+)\\s+\\[(?<op2>\\d+)\\]", _dec, 2)
        { }
    }

    class LiteralLiteral : Regexp
    {
        public LiteralLiteral(IRegexp _dec) : base("(?<op1>\\d+)\\s+(?<op2>\\d+)", _dec, 3)
        { }
    }

    class Address : Regexp
    {
        public Address(IRegexp _dec) : base("\\[(?<op>\\d+)\\]", _dec, 0)
        { }
    }

    class Literal : Regexp
    {
        public Literal(IRegexp _dec) : base("(?<op>\\d+)", _dec, 1)
        { }
    }

    class AddressAddressLiteral : Regexp
    {
        public AddressAddressLiteral(IRegexp _dec): base("\\[(?<op1>\\d+)\\]\\s+\\[(?<op2>\\d+)\\]\\s+(?<op3>\\d+)", _dec, 2)
        { }
    }

    class AddressAddressAddress : Regexp
    {
        public AddressAddressAddress(IRegexp _dec): base("\\[(?<op1>\\d+)\\]\\s+\\[(?<op2>\\d+)\\]\\s+\\[(?<op3>\\d+)\\]", _dec, 0)
        { }
    }

    class LiteralAddressAddress : Regexp
    {
        public LiteralAddressAddress(IRegexp _dec): base("(?<op1>\\d+)\\s+\\[(?<op2>\\d+)\\]\\s+\\[(?<op3>\\d+)\\]", _dec, 1)
        { }
    }

    class LiteralAddressLiteral : Regexp
    {
        public LiteralAddressLiteral(IRegexp _dec) : base("(?<op1>\\d+)\\s+\\[(?<op2>\\d+)\\]\\s+(?<op3>\\d+)", _dec, 3)
        { }
    }
    
    #endregion

    #region Instruction classes
    abstract class Instruction
    {
        protected byte Opcode { get; set; }
        protected abstract void Parse(string instructionArgs);
        protected IRegexp dec;
    }

    abstract class TwoArgInstruction : Instruction, IInstruction
    {
        protected byte op1;
        protected byte op2;
        protected byte offset;
        protected override void Parse(string instructionArgs)
        {
            Tuple<byte, Match> t;
            try
            {
                t = dec.Match(instructionArgs);
            }
            catch 
            {
                throw;
            }
            offset = t.Item1;
            op1 = byte.Parse(t.Item2.Groups["op1"].Value);
            op2 = byte.Parse(t.Item2.Groups["op2"].Value);
        }

        public string Decode()
        {
            return String.Format("0x{0:x} 0x{1:x} 0x{2:x}", Opcode + offset, op1, op2);
        }
    }

    abstract class OneArgInstruction : Instruction, IInstruction
    {
        protected byte op;
        protected byte offset;

        protected override void Parse(string instructionArgs)
        {
            var t = dec.Match(instructionArgs);
            offset = t.Item1;
            op = byte.Parse(t.Item2.Groups["op"].Value);
        }

        public string Decode()
        {
            return String.Format("0x{0:x} 0x{1:x}", Opcode + offset, op);
        }
    }

    abstract class ThreeArgInstruction : Instruction, IInstruction
    {
        protected byte op1;
        protected byte op2;
        protected byte op3;
        protected byte offset;

        protected override void Parse(string instructionArgs)
        {
            var t = dec.Match(instructionArgs);
            offset = t.Item1;
            op1 = byte.Parse(t.Item2.Groups["op1"].Value);
            op2 = byte.Parse(t.Item2.Groups["op2"].Value);
            op3 = byte.Parse(t.Item2.Groups["op3"].Value);
        }

        public string Decode()
        {
            return String.Format("0x{0:x} 0x{1:x} 0x{2:x} 0x{3:x}", Opcode + offset, op1, op2, op3);
        }
    }

    abstract class ZeroArgInstruction : Instruction, IInstruction
    {
        public string Decode()
        {
            return String.Format("0x{0:x}", Opcode);
        }

        protected override void Parse(string instructionArgs)
        {
            var inst = instructionArgs.Split(' ');
            if (inst.Length > 1)
            {
                throw new ArgumentException(String.Format("Invalid args - {0}", instructionArgs));
            }
        }
    }


    sealed class AND : TwoArgInstruction
    {
        public AND(string instruction)
        {
            Opcode = 0x00;
            dec = new AddressAddress(new AddressLiteral(null));
            Parse(instruction);
        }


    }

    sealed class OR : TwoArgInstruction
    {
        public OR(string instruction)
        {
            Opcode = 0x02;
            dec = new AddressAddress(new AddressLiteral(null));
            Parse(instruction);
        }

    }

    sealed class XOR : TwoArgInstruction
    {
        public XOR(string instruction)
        {
            Opcode = 0x04;
            dec = new AddressAddress(new AddressLiteral(null));
            Parse(instruction);
        }


    }

    sealed class NOT : OneArgInstruction
    {
        public NOT(string instruction)
        {
            Opcode = 0x06;
            dec = new Address(null);
            Parse(instruction);
        }
    }

    sealed class RANDOM : OneArgInstruction
    {
        public RANDOM(string instruction)
        {
            Opcode = 0x09;
            dec = new Address(null);
            Parse(instruction);
        }
    }

    sealed class MOV : TwoArgInstruction
    {
        public MOV(string instruction)
        {
            Opcode = 0x07;
            dec = new AddressAddress(new AddressLiteral(null));
            Parse(instruction);
        }
    }

    sealed class ADD : TwoArgInstruction
    {
        public ADD(string instruction)
        {
            Opcode = 0x0a;
            dec = new AddressAddress(new AddressLiteral(null));
            Parse(instruction);
        }
    }

    sealed class SUB : TwoArgInstruction
    {
        public SUB(string instruction)
        {
            Opcode = 0x0c;
            dec = new AddressAddress(new AddressLiteral(null));
            Parse(instruction);
        }
    }

    sealed class JZ : TwoArgInstruction
    {

        public JZ(string instruction)
        {
            Opcode = 0x10;
            dec = new AddressAddress(new AddressLiteral(new LiteralAddress(new LiteralLiteral(null))));
            Parse(instruction);
        }


    }

    sealed class JMP : OneArgInstruction
    {
        public JMP(string instruction)
        {
            Opcode = 0x0e;
            dec = new Address(new Literal(null));
            Parse(instruction);
        }
    }

    sealed class JEQ : ThreeArgInstruction
    {
        public JEQ(string instruction)
        {
            Opcode = 0x14;
            dec = new AddressAddressLiteral(new AddressAddressAddress(new LiteralAddressAddress(new LiteralAddressLiteral(null))));
            Parse(instruction);
        }


    }

    sealed class JLS : ThreeArgInstruction
    {
        public JLS(string instruction)
        {
            Opcode = 0x18;
            dec = new AddressAddressLiteral(new AddressAddressAddress(new LiteralAddressAddress(new LiteralAddressLiteral(null))));
            Parse(instruction);
        }


    }

    sealed class JGT : ThreeArgInstruction
    {
        public JGT(string instruction)
        {
            Opcode = 0x1c;
            dec = new AddressAddressLiteral(new AddressAddressAddress(new LiteralAddressAddress(new LiteralAddressLiteral(null))));
            Parse(instruction);
        }


    }

    sealed class APRINT : OneArgInstruction
    {
        public APRINT(string instruction)
        {
            Opcode = 0x20;
            dec = new Address(new Literal(null));
            Parse(instruction);
        }
    }

    sealed class DPRINT : OneArgInstruction
    {
        public DPRINT(string instruction)
        {
            Opcode = 0x22;
            dec = new Address(new Literal(null));
            Parse(instruction);
        }
    }

    sealed class HALT : ZeroArgInstruction
    {
        public HALT(string instruction)
        {
            Opcode = 0xff;
            Parse(instruction);
        }
    }
    #endregion

    class InstructionFactory
    {
        public static IInstruction GetInstruction(string instruction)
        {
            var inst = instruction.Split(new[]{' '}, 2);
            var mnemonic = inst[0];
            switch (mnemonic)
            {
                case "MOV":
                    return new MOV(instruction);
                case "AND":
                    return new AND(instruction);
                case "OR":
                    return new OR(instruction);
                case "XOR":
                    return new XOR(instruction);
                case "ADD":
                    return new ADD(instruction);
                case "SUB":
                    return new SUB(instruction);
                case "JZ":
                    return new JZ(instruction);
                case "RANDOM":
                    return new RANDOM(instruction);
                case "JMP":
                    return new JMP(instruction);
                case "NOT":
                    return new NOT(instruction);
                case "APRINT":
                    return new APRINT(instruction);
                case "DPRINT":
                    return new DPRINT(instruction);
                case "JEQ":
                    return new JEQ(instruction);
                case "JLS":
                    return new JLS(instruction);
                case "JGT":
                    return new JGT(instruction);
                case "HALT":
                    return new HALT(instruction);
                default:
                    throw new Exception(String.Format("Invalid instruction - {0}", instruction));
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("No file provided to translate.");
                Environment.Exit(-1);
            }

            foreach (var line in File.ReadAllLines(args[0]))
            {
                try
                {
                    Console.WriteLine(InstructionFactory.GetInstruction(line).Decode());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
