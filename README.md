This is an implementation of the challenge specified here:

http://www.reddit.com/r/dailyprogrammer/comments/1kqxz9/080813_challenge_132_intermediate_tiny_assembler/

Just compile the program 
and feed a file containing TinyASM assembly code.
